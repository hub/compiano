Compiano User's Manual
Version 0.9
September 2022

# Introduction

Compiano is a small application to turn your computer into a
synthesizer to play music. It offer X different instruments. You can
use the mouse, touchpad or the touch screen to press keys on the 25
keys virtual piano keyboard, but if you have a MIDI keyboard with USB
(virtually any MIDI keyboard controller on the market), you'll use
Compiano at its fuller potential.

# Requirements

- A Linux PC with Flatpak (only aarch64 and x86_64 are
  supported). Flatpak is the official distribution mechanism. If your
  favourite distribution includes Compiano in their packages, then it
  is fine too.
- Recommended: a USB MIDI controller. These go for less than CAD$100
  or 100€ for model with 25 keys and maybe some othe knobs. A standard
  MIDI USB device will work out of the box on Linux.
- Speaker or headphone.

# Getting started

Launch Compiano.

You get presented with the following window on your screen. This the
main user interface, you can use the virtual piano keyboard, select
your instrument, configure the audio or the MIDI controller.

The various elements of the UI:

![Main Window](main-window.png)

1. The main application menu
2. The status
3. Select your instrument
4. Configure your instrument. Currently only "soundfont" has such an option
5. View some info on the instrument.
6. The MIDI device for input
7. Open the preferences (same as in the application menu)
8. The virtual piano

