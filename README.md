Compiano
========

Compiano is a MIDI controllable software musical instrument. The name is a
portemanteau of "computer" and "piano"

I started this to:

1. learn about MIDI
2. explore some ideas

The goal are as follow:

1. make the app simple to use
2. have a large number of instrument (sound / tones) available
3. make it really first class MIDI controllable

The future:

1. This could potentially grow as a DAW or complete musician application
2. Or make it a LV2 plugin for existing DAW

Building
========

You can use GNOME Builder to build this, or use meson.

Requirements:

- gtk 4.4
- libadwaita 1.2
- libpanel

Note: for libpanel, the Rust bindings use a fork as a submodule as at
the time of writing there is no crate released compatible with this
version of gtk-rs.

The code is hosted on GNOME Gitlab:
https://gitlab.gnome.org/hub/compiano

FAQ
===

Q: There is already many of these, like VMPK, so why a new one ?

A: I love VMPK. I even made it available as a Flatpak on Flathub. But
sometime gotta have to start from scratch to explore ideas.

Q: How about Qwertone, it is written in Rust?

A: Speaking of which, it is the first Instrument available in Compiano. Many
thanks to the authors for the work I directly lifted.

Q: Why not fork existing apps to explore ideas ?

A: Compiano is written in Rust with Gtk4. That's the technological aspect.

Q: So what about the intruments ?

A: This is where I'll reuse as much as I kind. Lot of fantastic
software synthesizers available, just make them usable in one place.

Authors
=======

Hubert Figuière <hub@figuiere.net>

And other contributors and code borrow.

Qwertone synthesizer taken from Qwertone:
    https://gitlab.com/azymohliad/qwertone

License
=======

This is Free Software, licensed under GPLv3 (or later).
