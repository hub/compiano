// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2024 Hubert Figuière
//

use std::path::PathBuf;

use crossbeam_channel::{self, Receiver, Sender};
use fluidlite::{IsFont, IsPreset};
use midi_control as midi;

use crate::audio::AudioSource;
use crate::config;
use crate::instruments;
use crate::instruments::fluidlite_ui;
use crate::toolkit;

use super::{Plugin, SynthHandle};

#[derive(Clone)]
/// Sender for [`Message`] to the [`FluidLiteSynth`]
pub struct FluidLiteSynthHandle(pub Sender<Message>);

/// FluidLiteSynth message.
pub enum Message {
    /// MIDI Note on,
    NoteOn(midi::KeyEvent),
    /// MIDI Note off,
    NoteOff(midi::KeyEvent),
    /// MIDI pitch bend.
    PitchBend(u16),

    /// Load message
    Load,
    /// Load soundfont at the path
    LoadSoundFont(Option<PathBuf>),
    /// A soundbank with id `.0` is avalailable at `.1`.
    SoundbankAvailable(String, PathBuf),
    /// MIDI program change.
    ProgramChange(u8, u8),
}

/// A Single bank: list of presets (or voices). `.0` is the preset number,
/// `.1` is the user displayed preset name (or voice).
pub type Bank = Vec<(u8, String)>;

/// All the banks
pub struct Banks {
    /// The banks (in MIDI parlance). `.0` is the bank number, '.1` is the [`Bank`].
    pub banks: Vec<(u8, Bank)>,
}

impl Banks {
    /// Create an empty `Banks`
    pub fn new() -> Banks {
        Banks { banks: vec![] }
    }

    /// Add a `bank` with `num`.
    pub fn add_bank(&mut self, num: u8, bank: Bank) {
        self.banks.push((num, bank));
    }
}

impl SynthHandle for FluidLiteSynthHandle {
    fn load(&self) {
        print_on_err!(self.0.send(Message::Load));
    }

    fn note_on(&self, event: &midi::KeyEvent) {
        print_on_err!(self.0.send(Message::NoteOn(event.clone())));
    }

    fn note_off(&self, event: &midi::KeyEvent) {
        print_on_err!(self.0.send(Message::NoteOff(event.clone())));
    }

    fn pitch_bend(&self, value: u16) {
        print_on_err!(self.0.send(Message::PitchBend(value)));
    }

    fn get_name(&self) -> String {
        "FluidLite".to_string()
    }

    fn is_velocity_capable(&self) -> bool {
        true
    }

    fn soundbank_available(&self, soundbank: &str, p: &std::path::Path) {
        log::debug!("Got soundbank {} at {:?}", soundbank, p);
        print_on_err!(self
            .0
            .send(Message::SoundbankAvailable(soundbank.into(), p.into())));
    }
}

/// Just load the specified soundfont at `path`.
///
/// If loaded, this will reset the "program".
///
/// `synth`: the [fluidlite synthesizer][`fluidlite::Synth`].
///
/// Returns the [font ID][`fluidlite::FontId`] for use later.
///
/// XXX return an actual Result.
fn synth_load_soundfont(
    synth: &fluidlite::Synth,
    path: Option<PathBuf>,
) -> Option<fluidlite::FontId> {
    path.and_then(|path| synth.sfload(path, true).ok())
        .inspect(|_| {
            print_on_err!(synth.program_reset());
        })
}

const SAMPLES_SIZE: usize = crate::audio::DEFAULT_RATE as usize; //2205 * 2;

/// A synth using fluidlite to play soundfonts. `.sf` and `.sf2`.
pub struct FluidLiteSynth {
    /// The optional UI handle.
    ///
    /// For `FluidLiteSynth`, when used with a preset soundfont, there is no UI.
    ui: Option<async_channel::Sender<fluidlite_ui::Message>>,
    /// The synth
    synth: fluidlite::Synth,
    /// The messages receiver for [`Message`].
    messages: Receiver<Message>,
    /// The optional soundfont name.
    soundfont: Option<String>,
    /// The name of the soundbank expected, if any.
    soundbank_name: Option<String>,
    /// Current selected [`Bank`].
    bank: i64,
    /// Current selected voice in the `Bank`.
    preset_num: i64,
    /// Audio samples buffer.
    samples: [f32; SAMPLES_SIZE],
    /// Current reading position in the `samples` buffer.
    pos: usize,
}

impl AudioSource for FluidLiteSynth {
    fn get_samples(&mut self, n_frames: usize) -> [&[f32]; 2] {
        self.fill_buffer(n_frames);
        [&self.samples, &self.samples]
    }

    fn process_messages(&mut self) {
        loop {
            match self.messages.try_recv() {
                Ok(Message::Load) => {
                    if let Some(ref soundfont) = self.soundfont {
                        let soundbank_dir = /*if let Some(soundbank) = self.preset.soundbank {
                            soundbank.path
                        } else */ {
                            PathBuf::from(config::PKGDATADIR).join("soundfonts")
                        };
                        let sf_path = soundbank_dir.join(soundfont);
                        if let Some(font_id) = synth_load_soundfont(&self.synth, Some(sf_path)) {
                            print_on_err!(self.synth.program_select(
                                0,
                                font_id,
                                self.bank as u32,
                                self.preset_num as u32
                            ));
                        } else {
                            log::error!("Couldn't load soundfont");
                        }
                    }
                }
                Ok(Message::NoteOn(e)) => {
                    print_on_err!(self.synth.note_on(0, e.key.into(), e.value.into()));
                }
                Ok(Message::NoteOff(e)) => {
                    print_on_err!(self.synth.note_off(0, e.key.into()));
                }
                Ok(Message::PitchBend(val)) => {
                    print_on_err!(self.synth.pitch_bend(0, val as u32));
                }
                Ok(Message::SoundbankAvailable(soundbank, mut path)) => {
                    if self.soundbank_name == Some(soundbank) {
                        if let Some(ref soundfont) = self.soundfont {
                            path.push(soundfont);
                            if let Some(font_id) = self.load_soundfont(Some(path)) {
                                print_on_err!(self.synth.program_select(
                                    0,
                                    font_id,
                                    self.bank as u32,
                                    self.preset_num as u32
                                ));
                            }
                        }
                    }
                }
                Ok(Message::LoadSoundFont(path)) => {
                    self.load_soundfont(path).map(|fontid| {
                        self.list_banks_and_presets(fontid).map(|banks| {
                            if let Some(sender) = &self.ui {
                                let sender = sender.clone();
                                toolkit::utils::send_async_any!(
                                    fluidlite_ui::Message::Banks(banks),
                                    sender
                                );
                            }
                        })
                    });
                }
                Ok(Message::ProgramChange(b, p)) => {
                    // XXX properly handle channel. We currently assume 0.
                    if let Some(f) = self.synth.get_sfont(0).map(|f| f.get_id()) {
                        print_on_err!(self.synth.program_select(0, f, b as u32, p as u32));
                    }
                }
                Err(_) => break,
            }
        }
    }
}

/// crossbeam channel size.
const CHANNEL_SIZE: usize = 256;

impl FluidLiteSynth {
    /// Create a `FluidLiteSynth`
    ///
    /// `sample_rate` in Hz
    ///
    /// `preset`: the [`Preset`][instruments::Preset]
    pub fn create(sample_rate: u32, preset: &instruments::Preset) -> super::Result<Plugin> {
        let (tx, messages) = crossbeam_channel::bounded(CHANNEL_SIZE);
        let settings = fluidlite::Settings::new().map_err(|_| {
            super::Error::InitializationFailed("Couldn't create fluidlite settings.")
        })?;
        let synth = fluidlite::Synth::new(settings)
            .map_err(|_| super::Error::InitializationFailed("Couldn't create fluidlite synth"))?;
        synth.set_sample_rate(sample_rate as f32);
        synth.set_gain(4.0);

        let mut soundfont = None;
        let soundbank_name = preset.soundbank.clone();
        let mut bank = 0;
        let mut preset_num = 0;
        if let Some(params) = preset.params.as_ref() {
            soundfont = params.get("soundfont").and_then(|v| match v {
                instruments::ParamValue::Str(soundfont) => Some(soundfont.clone()),
                _ => None,
            });
            bank = params
                .get("bank")
                .and_then(instruments::ParamValue::to_int)
                .unwrap_or(0);
            preset_num = params
                .get("preset")
                .and_then(instruments::ParamValue::to_int)
                .unwrap_or(0);
        }
        // XXX only do that if necessary.
        let (sender, receiver) = async_channel::unbounded::<fluidlite_ui::Message>();
        let samples = [0.0f32; SAMPLES_SIZE];
        let this = FluidLiteSynth {
            ui: Some(sender),
            synth,
            messages,
            soundfont,
            soundbank_name,
            bank,
            preset_num,
            samples,
            pos: 0,
        };
        let handle = FluidLiteSynthHandle(tx);
        Ok(if let Some(ref ui_name) = preset.ui {
            Plugin::with_ui(
                Box::new(fluidlite_ui::FluidLiteUi::new(ui_name, receiver, &handle)),
                Box::new(this),
                Box::new(handle),
            )
        } else {
            Plugin::new(Box::new(this), Box::new(handle))
        })
    }

    /// Load the soundfont, removing the curently loaded one.
    ///
    /// `path` the path where the soundfont file is. If `None` then the current font is just
    /// unloaded.
    ///
    /// Returns the [font ID][fluidlite::FontId].
    fn load_soundfont(&self, path: Option<PathBuf>) -> Option<fluidlite::FontId> {
        // empty the soundfont stack.
        while let Some(sfont) = self.synth.get_sfont(0) {
            self.synth.remove_sfont(sfont);
        }
        synth_load_soundfont(&self.synth, path)
    }

    /// List the banks and presets for the font.
    ///
    /// `fontid`: the [font ID][fluidlite::FontId] to list from.
    ///
    /// Return the [`Banks`]. This is what is passed with through the
    /// [`Banks` UI message][fluidlite_ui::Message]
    fn list_banks_and_presets(&self, fontid: fluidlite::FontId) -> Option<Banks> {
        self.synth.get_sfont_by_id(fontid).map(|font| {
            let mut banks = Banks::new();
            for b in 0..=127 {
                let mut bank: Bank = vec![];
                for p in 0..=127 {
                    if let Some(preset) = font.get_preset(b, p) {
                        let name = preset.get_name().unwrap_or("NOPRESETNAME");
                        bank.push((p as u8, name.into()));
                    }
                }
                if !bank.is_empty() {
                    banks.add_bank(b as u8, bank);
                }
            }

            banks
        })
    }

    /// Fill the buffer of samples from fluidlite, and reset the position.
    fn fill_buffer(&mut self, n_frames: usize) {
        print_on_err!(self.synth.write(self.samples[0..n_frames].as_mut()));
        self.pos = 0;
    }
}
