// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2024 Hubert Figuière
//

use midi_control as midi;

use crate::audio::AudioSource;
use crate::instruments;

use super::qwertone;
use super::{Plugin, SynthHandle};

#[derive(Clone)]
/// Handle to [`QwertoneSynth`].
pub struct QwertoneSynthHandle(qwertone::SynthesizerHandle);

impl SynthHandle for QwertoneSynthHandle {
    fn load(&self) {}

    fn note_on(&self, e: &midi::KeyEvent) {
        let note = e.key;
        if note >= 36 {
            print_on_err!(self.0.send(qwertone::Message::NoteHold(note as usize - 36)));
        }
    }

    fn note_off(&self, e: &midi::KeyEvent) {
        let note = e.key;
        if note >= 36 {
            print_on_err!(self
                .0
                .send(qwertone::Message::NoteRelease(note as usize - 36)));
        }
    }

    fn pitch_bend(&self, _value: u16) {}

    fn get_name(&self) -> String {
        "Qwertone".to_owned()
    }

    fn is_velocity_capable(&self) -> bool {
        false
    }

    // Qwertone has no soundbank
    fn soundbank_available(&self, _: &str, _: &std::path::Path) {}
}

/// Qwertone Synth
///
/// This synthesizer was extracted from [qwertone]<https://gitlab.com/azymohliad/qwertone>, which
/// was also an inspiration for the initial architecture of the sound engine.
pub struct QwertoneSynth(qwertone::Synthesizer);

impl AudioSource for QwertoneSynth {
    fn get_samples(&mut self, n_frames: usize) -> [&[f32]; 2] {
        self.0.get_samples(n_frames)
    }

    fn process_messages(&mut self) {
        self.0.process_messages();
    }
}

impl QwertoneSynth {
    pub fn create(sample_rate: u32, _desc: &instruments::Preset) -> super::Result<Plugin> {
        // This is an arbtrary number linked to the implementation of Qwertone synth.
        // Note 0 in this synth is MIDI note 36. So (max MIDI) 127 - 36 = 91.
        let (synthesizer, synthesizer_handle) = qwertone::Synthesizer::new(91, sample_rate);
        Ok(Plugin {
            ui: None,
            synth: Box::new(Self(synthesizer)),
            handle: Box::new(QwertoneSynthHandle(synthesizer_handle)),
        })
    }
}
