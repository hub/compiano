// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2023 Hubert Figuière
//

mod archive;
mod downloader;
mod file;
mod git;

use std::collections::HashMap;
use std::str::FromStr;
use std::sync::Mutex;

use once_cell::sync::Lazy;
use serde::de;
use serde_derive::Deserialize;

pub use downloader::DownloadManager;
pub use downloader::DownloaderResult;
pub use downloader::Error;

#[derive(Clone, Debug, Eq, PartialEq)]
/// Type of soundbank archive to download.
/// This is not the format of the soundbank.
pub enum Type {
    /// Soundbank is a Git repository
    Git,
    /// Soundbank is an archive
    Archive,
    /// Soundbank is a plain file
    File,
}

impl FromStr for Type {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "git" => Ok(Type::Git),
            "archive" => Ok(Type::Archive),
            "file" => Ok(Type::File),
            _ => Err(()),
        }
    }
}

impl<'de> serde::de::Deserialize<'de> for Type {
    fn deserialize<D>(deserializer: D) -> Result<Type, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        deserializer.deserialize_any(TypeVisitor)
    }
}

struct TypeVisitor;

impl<'de> serde::de::Visitor<'de> for TypeVisitor {
    type Value = Type;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "a Type")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Type::from_str(s).map_err(|_| {
            de::Error::invalid_value(de::Unexpected::Str(s), &"'archive', 'file', or 'git'")
        })
    }
}

#[derive(Clone, Debug, Deserialize)]
/// A soundbank, ie the (maybe downloaded) data.
pub struct Soundbank {
    /// id, also used in building the download path.
    id: String,
    /// visible name
    pub name: String,
    /// path to the download
    pub path: Option<std::path::PathBuf>,
    /// archive type
    r#type: Type,
    /// url to download it
    url: Option<String>,
    /// sha256 checksum (type == Archive)
    sha256: Option<String>,
    /// commit (type == Git)
    commit: Option<String>,
}

/// The list of [`Soundbank`]. Key is the `id`.
pub type SoundbankList = HashMap<String, Soundbank>;

/// Load the soundbank list from the resource bytes, in toml format.
fn load_soundbank_list<B>(data: B) -> SoundbankList
where
    B: AsRef<str>,
{
    match toml::from_str::<SoundbankList>(data.as_ref()) {
        Ok(list) => list,
        Err(err) => {
            eprintln!("Error parsing: {err}");
            HashMap::new()
        }
    }
}

/// Global soundbank registry loaded from the resource, from toml data.
pub static SOUNDBANK_REGISTRY: Lazy<Mutex<SoundbankList>> = Lazy::new(|| {
    #[cfg(not(test))]
    {
        use gtk4::gio;

        let bytes = gio::resources_lookup_data(
            "/net/figuiere/compiano/soundbanks/soundbanks.toml",
            gio::ResourceLookupFlags::NONE,
        )
        .expect("Can't load instrument list from resources");
        Mutex::new(load_soundbank_list(
            String::from_utf8(bytes.to_vec()).unwrap(),
        ))
    }
    #[cfg(test)]
    {
        Mutex::new(load_soundbank_list(include_str!("tests/soundbanks.toml")))
    }
});

#[cfg(test)]
mod tests {

    use super::Type;

    #[test]
    fn test_soundbank_loading() {
        let list = super::load_soundbank_list(include_str!("tests/soundbanks.toml"));

        assert!(!list.is_empty());
        assert_eq!(list.len(), 3);

        let sso = list.get("sonatinaso");
        assert!(sso.is_some());
        let sso = sso.unwrap();
        assert_eq!(sso.id, "sonatinaso");
        assert_eq!(sso.r#type, Type::Archive);
        assert!(sso.commit.is_none());
        assert!(sso.sha256.is_some());

        let rhodes = list.get("rhodes3c");
        assert!(rhodes.is_some());
        let rhodes = rhodes.unwrap();
        assert_eq!(rhodes.id, "rhodes3c");
        assert_eq!(rhodes.r#type, Type::Git);
        assert!(rhodes.commit.is_some());
        assert!(rhodes.sha256.is_none());

        let egp = list.get("electric-gp");
        assert!(egp.is_some());
        let egp = egp.unwrap();
        assert_eq!(egp.id, "electric-gp");
        assert_eq!(egp.r#type, Type::File);
        assert!(egp.sha256.is_some());
    }
}
