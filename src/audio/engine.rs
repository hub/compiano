// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2025 Hubert Figuière
//

use anyhow::Result;
use pipewire::{
    context::Context,
    keys,
    main_loop::MainLoop,
    properties::properties,
    spa,
    stream::{Stream, StreamFlags, StreamRef},
};
use spa::{buffer::Data, pod::Pod, utils::Direction};

use super::AudioSource;

const CHAN_SIZE: usize = std::mem::size_of::<f32>();

/// The audio engine, takes a source and output to the sound subsystem.
pub struct Engine(std::thread::JoinHandle<()>);

impl Engine {
    /// Returns the sample rate associated to the output format.
    pub fn sample_rate(&self) -> u32 {
        super::DEFAULT_RATE
    }

    /// Start the audio engine and run it.
    ///
    /// It is run on a different thread, hence the requirement for the [`AudioSource`]
    /// to be also `Send`.
    pub fn start(mut source: Box<dyn AudioSource + Send>) -> Result<Engine> {
        pipewire::init();

        let engine = Engine(
            std::thread::Builder::new()
                .name("audio engine loop".to_string())
                .spawn(move || {
                    let mainloop = MainLoop::new(None).expect("Failed to create main loop");
                    let context = Context::new(&mainloop).expect("Failed to create context");
                    if let Ok(core) = context.connect(None) {
                        // let registry = core.get_registry()?;
                        let stream = Stream::new(
                            &core,
                            "compiano synth",
                            properties! {
                                *keys::MEDIA_TYPE => "Audio",
                                *keys::MEDIA_CATEGORY => "Playback",
                                *keys::MEDIA_ROLE => "Music"
                            },
                        )
                        .expect("Failed to create stream");

                        let _listener = stream
                            .add_local_listener()
                            .process(move |stream: &StreamRef, _: &mut ()| {
                                match stream.dequeue_buffer() {
                                    None => println!("No buffer"),
                                    Some(mut b) => {
                                        source.process_messages();
                                        let datas = b.datas_mut();
                                        let data = &mut datas[0];
                                        write_data(data, &mut source);
                                    }
                                }
                            })
                            .register()
                            .expect("Couldn't register listener");

                        let mut audio_info = spa::param::audio::AudioInfoRaw::new();
                        audio_info.set_format(spa::param::audio::AudioFormat::F32LE);
                        audio_info.set_rate(super::DEFAULT_RATE);
                        audio_info.set_channels(super::DEFAULT_CHANNELS as u32);

                        let values: Vec<u8> = spa::pod::serialize::PodSerializer::serialize(
                            std::io::Cursor::new(Vec::new()),
                            &spa::pod::Value::Object(spa::pod::Object {
                                type_: spa::sys::SPA_TYPE_OBJECT_Format,
                                id: spa::sys::SPA_PARAM_EnumFormat,
                                properties: audio_info.into(),
                            }),
                        )
                        .unwrap()
                        .0
                        .into_inner();

                        let mut params = [Pod::from_bytes(&values).unwrap()];

                        stream
                            .connect(
                                Direction::Output,
                                None,
                                StreamFlags::AUTOCONNECT
                                    | StreamFlags::MAP_BUFFERS
                                    | StreamFlags::RT_PROCESS,
                                &mut params,
                            )
                            .expect("Connect failed");
                        mainloop.run();
                    }
                })?,
        );
        Ok(engine)
    }
}

fn write_data(output: &mut Data, source: &mut Box<dyn AudioSource + Send>) {
    let stride = super::DEFAULT_CHANNELS * CHAN_SIZE;
    let n_frames = if let Some(data) = output.data() {
        let n_frames = data.len() / stride;
        // XXX hardcoded to 2 in get_samples().
        let values = source.get_samples(n_frames);
        for f in 0..n_frames {
            for (i, channel) in values.iter().enumerate().take(super::DEFAULT_CHANNELS) {
                let start = f * stride + i * CHAN_SIZE;
                let chan = &mut data[start..start + CHAN_SIZE];
                chan.copy_from_slice(&f32::to_le_bytes(channel[f]));
            }
        }
        n_frames
    } else {
        0
    };
    let chunk = output.chunk_mut();
    *chunk.offset_mut() = 0;
    *chunk.stride_mut() = stride as _;
    *chunk.size_mut() = (stride * n_frames) as _;
}
