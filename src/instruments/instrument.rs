// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2022 Hubert Figuière
//

use log::debug;

use super::Preset;
use crate::soundbanks;
use crate::soundbanks::Soundbank;
use crate::synth;
use crate::synth::Plugin;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
/// Instrument state.
pub enum State {
    /// Instrument is ready.
    Ready,
    /// Instrument needs a soundbank to be downloaded.
    NeedSoundbank,
    /// Instrument waiting for soundbank to be downloaded.
    Downloading,
}

/// The actual instrument
///
/// It represents an instance of a [preset][Preset], either factory or user.
pub struct Instrument {
    /// state of the instrument
    state: State,
    /// id of the preset
    preset_id: String,
    /// name for the instrument instance. Default to the preset name
    name: String,
    /// Soundbank for the instrument if needed.
    pub soundbank: Option<Soundbank>,

    pub plugin: Plugin,
}

impl Instrument {
    /// Create an [`Instrument`] for a preset.
    ///
    /// `preset`: the [`Preset`].
    ///
    /// `sample_rate`: the sample rate in Hz.
    pub fn new(preset: &Preset, sample_rate: u32) -> Option<Instrument> {
        debug!("Preset soundbank {:?}", preset.soundbank);
        let soundbank = preset.soundbank.as_ref().and_then(|s| {
            soundbanks::SOUNDBANK_REGISTRY
                .lock()
                .ok()
                .and_then(|h| h.get(s).cloned())
        });

        synth::SYNTH_REGISTRY
            .lock()
            .unwrap()
            .get(&preset.synth)
            .and_then(|factory| {
                factory(sample_rate, preset)
                    .map_err(|e| {
                        log::error!("Failed to create synth '{}': {:?}", &preset.synth, e);
                        e
                    })
                    .ok()
            })
            .map(|plugin| {
                debug!("Sounbank for preset {} is {:?}", preset.id, &soundbank);
                Instrument {
                    state: if soundbank.is_none() {
                        State::Ready
                    } else {
                        State::NeedSoundbank
                    },
                    preset_id: preset.id.clone(),
                    name: preset.name.clone(),
                    soundbank,
                    plugin,
                }
            })
    }

    /// Returns the instrument state
    pub fn state(&self) -> State {
        self.state
    }
}

#[cfg(test)]
mod tests {
    use super::{Instrument, State};
    use crate::instruments::load_preset_list;

    #[test]
    fn test_instrument() {
        let list = load_preset_list(include_str!("tests/presets.toml"));

        let preset = list.get("steinway");
        assert!(preset.is_some());
        let preset = preset.unwrap();

        let instrument = Instrument::new(preset, crate::audio::DEFAULT_RATE);
        assert!(instrument.is_some());
        let instrument = instrument.unwrap();
        assert!(instrument.soundbank.is_some());
        assert_eq!(instrument.state(), State::NeedSoundbank);
        let soundbank = instrument.soundbank.unwrap();
        assert!(soundbank.path.is_none()); // TODO fix this, should be Some()

        let preset2 = list.get("pipeorgan");
        assert!(preset2.is_some());
        let preset2 = preset2.unwrap();

        let instrument2 = Instrument::new(preset2, crate::audio::DEFAULT_RATE);
        assert!(instrument2.is_some());
        let instrument2 = instrument2.unwrap();
        assert!(instrument2.soundbank.is_none());
        assert_eq!(instrument2.state(), State::Ready);
    }
}
