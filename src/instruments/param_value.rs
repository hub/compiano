// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2022 Hubert Figuière
//

use serde::de;

#[derive(Clone, Debug, PartialEq)]
/// Parameter value for [`Params`][super::Params].
pub enum ParamValue {
    Str(String),
    Int(i64),
    Float(f64),
    Bool(bool),
}

impl ParamValue {
    /// Convert to a `str` if possible.
    pub fn to_str(&self) -> Option<&str> {
        match *self {
            Self::Str(ref s) => Some(s),
            _ => None,
        }
    }

    /// Convert to an integer `i64` if possible
    ///
    /// Will attempt to convert a string to an integer.
    pub fn to_int(&self) -> Option<i64> {
        match *self {
            Self::Int(n) => Some(n),
            Self::Str(ref s) => s.parse::<i64>().ok(),
            _ => None,
        }
    }
}

impl<'de> serde::de::Deserialize<'de> for ParamValue {
    fn deserialize<D>(deserializer: D) -> Result<ParamValue, D::Error>
    where
        D: serde::de::Deserializer<'de>,
    {
        deserializer.deserialize_any(ParamValueVisitor)
    }
}

struct ParamValueVisitor;

impl<'de> serde::de::Visitor<'de> for ParamValueVisitor {
    type Value = ParamValue;

    fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(formatter, "an ParamValue")
    }

    fn visit_str<E>(self, s: &str) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(ParamValue::Str(s.to_owned()))
    }

    fn visit_bool<E>(self, v: bool) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(ParamValue::Bool(v))
    }

    fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(ParamValue::Int(v))
    }

    fn visit_f64<E>(self, v: f64) -> Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(ParamValue::Float(v))
    }
}
