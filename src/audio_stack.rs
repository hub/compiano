// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2024 Hubert Figuière
//

use std::cell::RefCell;
use std::sync::Arc;

use gtk4::glib;
use i18n_format::i18n_fmt;
use log::debug;

use crate::audio;
use crate::events::Message;
use crate::instruments;
use crate::instruments::Preset;
use crate::midi::engine::{MidiControlMessage, MidiControlSender};
use crate::soundbanks;
use crate::soundbanks::DownloadManager;
use crate::synth::BoxedHandle;
use crate::toolkit;

/// Messages sent back the audio stack.
pub enum AudioMessage {
    /// A soundbank has been made available (download finished)
    SoundbankAvailable(String, std::path::PathBuf),
}

/// The audio stack (controller)
///
/// The audio stack responsibilities range from:
/// * owning the [audio engine][audio::Engine] and the [mixer][audio::Mixer]
/// * owning the [MIDI control][MidiControl]
/// * spawning the [download manager][DownloadManager] for soundbanks.
/// * instantiating a synth from a [`Preset`]
/// * Requesting the application to display the UI with a [`Message`].
pub struct AudioStack {
    /// The audio engine.
    _engine: RefCell<audio::Engine>,
    /// MIDI control sender.
    pub midi_control: MidiControlSender,
    /// The mixer handle.
    mixer: audio::MixerHandle,
    /// sample rate in Hz.
    sample_rate: u32,
    /// To send messages to the audio stack.
    pub sender: async_channel::Sender<AudioMessage>,
}

impl AudioStack {
    /// Create a new audio stack with a MidiControl
    pub fn new(midi_control: MidiControlSender) -> Arc<AudioStack> {
        let (mixer, mixer_handle) = audio::Mixer::new();

        let (sender, receiver) = async_channel::unbounded::<AudioMessage>();

        let engine = audio::Engine::start(Box::new(mixer)).expect("Failed to start audio engine.");
        let sample_rate = engine.sample_rate();

        let stack = Arc::new(AudioStack {
            _engine: RefCell::new(engine),
            midi_control,
            sample_rate,
            mixer: mixer_handle,
            sender,
        });

        glib::spawn_future_local(glib::clone!(
            #[strong]
            stack,
            async move {
                while let Ok(msg) = receiver.recv().await {
                    stack.messages(msg);
                }
            }
        ));

        stack
    }

    /// Set the synth to the one defined by `preset`.
    ///
    /// `sender` is the channel to send [`Message`] back to the UI.
    ///
    /// XXX return an error when applicable
    pub fn set_synth(&self, preset: &Preset, sender: &async_channel::Sender<Message>) {
        let synth = self
            .make_synth(preset)
            .map(|(synth_handle, instrument_ui)| {
                if let Some(ui) = &preset.ui {
                    let sender = sender.clone();
                    let ui = ui.to_string();
                    toolkit::utils::send_async_local!(
                        Message::ShowInstrumentUi(ui, instrument_ui),
                        sender
                    );
                }
                synth_handle
            });
        if synth.is_none() {
            crate::ui::display_message_local(i18n_fmt! {
                // Translators: "{}" will be replaced by the name of the preset.
                // Do not remove.
                i18n_fmt("Failed to create synth for '{}'", &preset.name)
            });
        }
        print_on_err!(self.midi_control.send(MidiControlMessage::SetSynth(synth)));
    }

    /// Instantiate a synth out on an instrument [`Preset`].
    ///
    /// Returns a tuple of the synth Handle and the Synth UI.
    fn make_synth(
        &self,
        preset: &instruments::Preset,
    ) -> Option<(BoxedHandle, Option<Box<dyn instruments::InstrumentUi>>)> {
        instruments::Instrument::new(preset, self.sample_rate).map(|instrument| {
            debug!("Instrument created. State: {:?}", instrument.state());

            let state = instrument.state();

            if state == instruments::InstrumentState::NeedSoundbank {
                if let Some(soundbank) = instrument.soundbank.as_ref().cloned() {
                    // Check and download if needed.
                    let sender = self.sender.clone();
                    debug!("Spawning download manager");
                    print_on_err!(std::thread::Builder::new()
                        .name("soundbank downloader".to_string())
                        .spawn(move || {
                            let manager = DownloadManager::new(sender);
                            match manager.download(&soundbank) {
                                // Ignore the already downloaded error.
                                Err(soundbanks::Error::AlreadyDownloaded) => {}
                                err => print_on_err!(err),
                            }
                        }));
                }
            }

            let plugin = instrument.plugin;
            print_on_err!(self
                .mixer
                .send(audio::MixerMessage::ReplaceSynth((plugin.synth, 0.5))));
            if state == instruments::InstrumentState::Ready {
                plugin.handle.load();
            }
            (plugin.handle, plugin.ui)
        })
    }

    fn messages(&self, msg: AudioMessage) {
        use AudioMessage::*;
        match msg {
            SoundbankAvailable(soundbank, path) => {
                println!("Soundbank {soundbank} is available at {path:?}");
                // signal the instrument.
                print_on_err!(self
                    .midi_control
                    .send(MidiControlMessage::SoundbankAvailable(soundbank, path)));
            }
        }
    }
}
