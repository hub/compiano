// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2024 Hubert Figuière
//

use midi_control as midi;
use once_cell::sync::OnceCell;

use crate::instruments;
use crate::window;

pub static SENDER: OnceCell<async_channel::Sender<Message>> = OnceCell::new();

/// UI messages
pub enum Message {
    /// MIDI Device has been selected. Contain the port index and name.
    DeviceSelected(String),
    /// MIDI Channel has been selected. Contain the MIDI Channel.
    ChannelSelected(midi::Channel),
    /// A piano key is down
    PianoKeyDown(u8),
    /// A piano key is up
    PianoKeyUp(u8),
    /// An instrument was select. Contain the instrument id (string).
    InstrumentSelected(String),
    /// Show instrument UI, sent by the [`AudioStack`][crate::audio_stack::AudioStack]
    ShowInstrumentUi(String, Option<Box<dyn instruments::InstrumentUi>>),
    KeyboardShortcuts,
    Preferences,
    PreferencesClosed(usize),
    /// To display an error message.
    DisplayMsg(String),
}

/// Dispatch the event.
///
/// This is basically grand central for all the UI commands
pub fn dispatch(event: Message, window: &window::WindowController) {
    use Message::*;
    match event {
        DeviceSelected(s) => window.midi_device_selected(&s),
        ChannelSelected(ch) => window.channel_selected(ch),
        PianoKeyDown(note) => window.piano_key_down(note),
        PianoKeyUp(note) => window.piano_key_up(note),
        InstrumentSelected(s) => window.instrument_selected(&s),
        ShowInstrumentUi(s, ui) => window.show_instrument_ui(&s, ui),
        KeyboardShortcuts => {}
        Preferences => window.show_preferences(),
        PreferencesClosed(id) => window.remove_controller(id),
        DisplayMsg(msg) => window.display_message(&msg),
    }
}
