# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: (c) 2020-2022 Hubert Figuière

pkgdatadir = join_paths(get_option('prefix'), get_option('datadir'), meson.project_name())
gnome = import('gnome')

resources = gnome.compile_resources('compiano',
  'compiano.gresource.xml',
  gresource_bundle: true,
  install: true,
  install_dir: pkgdatadir,
)

conf = configuration_data()
conf.set_quoted('VERSION', meson.project_version())
conf.set_quoted('localedir', join_paths(get_option('prefix'), get_option('localedir')))
conf.set_quoted('pkgdatadir', pkgdatadir)
conf.set_quoted('datadir', join_paths(get_option('prefix'), get_option('datadir')))
conf.set_quoted('PROFILE', profile)
conf.set_quoted('GETTEXT_PACKAGE', gettext_package)

configure_file(
    input: 'config.rs.in',
    output: 'config.rs',
    configuration: conf
)

# Copy the config.rs output to the source directory.
run_command(
  'cp',
  join_paths(meson.project_build_root(), 'src', 'config.rs'),
  join_paths(meson.project_source_root(), 'src', 'config.rs'),
  check: true
)

sources = files(
  join_paths('..', 'Cargo.toml'),
  'audio_stack.rs',
  join_paths('audio', 'engine.rs'),
  join_paths('audio', 'mixer.rs'),
  join_paths('audio', 'mod.rs'),
  'config.rs',
  'events.rs',
  join_paths('instruments', 'fluidlite_ui.rs'),
  join_paths('instruments', 'instrument.rs'),
  join_paths('instruments', 'mod.rs'),
  join_paths('instruments', 'param_value.rs'),
  join_paths('instruments', 'ui.rs'),
  'main.rs',
  join_paths('midi', 'engine.rs'),
  join_paths('midi', 'mod.rs'),
  'settings.rs',
  join_paths('soundbanks', 'archive.rs'),
  join_paths('soundbanks', 'downloader.rs'),
  join_paths('soundbanks', 'file.rs'),
  join_paths('soundbanks', 'git.rs'),
  join_paths('soundbanks', 'mod.rs'),
  join_paths('synth', 'fluidlite_synth.rs'),
  join_paths('synth', 'mod.rs'),
  join_paths('synth', 'qwertone_synth.rs'),
  join_paths('synth', 'qwertone', 'amplitude.rs'),
  join_paths('synth', 'qwertone', 'mod.rs'),
  join_paths('synth', 'qwertone', 'tone.rs'),
  join_paths('synth', 'qwertone', 'utils.rs'),
  join_paths('toolkit', 'filemanager.rs'),
  join_paths('toolkit', 'mod.rs'),
  join_paths('ui', 'dialog_controller.rs'),
  join_paths('ui', 'mod.rs'),
  join_paths('ui', 'preferences.rs'),
  'utils.rs',
  join_paths('widgets', 'file_chooser_button.rs'),
  join_paths('widgets', 'instrument_info_popover.rs'),
  join_paths('widgets', 'mod.rs'),
  join_paths('widgets', 'piano_widget.rs'),
  'window.rs',
)

cargo_options = [ '--manifest-path', meson.project_source_root() / 'Cargo.toml' ]
cargo_options += [ '--target-dir', meson.project_build_root() / 'src' ]

if get_option('profile') == 'default'
  cargo_options += [ '--release' ]
  rust_target = 'release'
  message('Building in release mode')
else
  rust_target = 'debug'
  message('Building in debug mode')
endif

cargo_env = [ 'CARGO_HOME=' + meson.project_build_root() / 'cargo-home' ]

cargo_build = custom_target(
  'cargo-build',
  build_by_default: true,
  build_always_stale: true,
  output: meson.project_name(),
  console: true,
  install: true,
  install_dir: bindir,
  depends: resources,
  command: [
    'env',
    cargo_env,
    cargo, 'build',
    cargo_options,
    '&&',
    'cp', 'src' / rust_target / meson.project_name(), '@OUTPUT@',
  ]
)

test(
  'cargo-test',
  cargo,
  env: cargo_env + 'RUST_LOG=debug',
  timeout: 60,
  args: [
    'test',
    cargo_options,
    '--',
    '--nocapture'
  ]
)
